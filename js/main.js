// Global Variables
let width = 500,
    height = 0,
    filter = 'none',
    streaming = false

// DOM Elements
const video = document.getElementById('video')
const canvas = document.getElementById('canvas')
const photos = document.getElementById('photos')
const photoButton = document.getElementById('photo-button')
const clearButton = document.getElementById('clear-button')
const photoFilter = document.getElementById('photo-filter')

// Get Media Stream
navigator.mediaDevices
    .getUserMedia({ video: true, audio: false })
    .then((stream) => {
        // Link to the video src
        video.srcObject = stream
        video.play()
    })
    .catch((err) => {
        console.log(`Error: ${err}`)
    })

// Play when ready
video.addEventListener(
    'canplay',
    (e) => {
        if (!streaming) {
            // Set video canvas height
            height = video.videoHeight / (video.videoWidth / width)

            video.setAttribute('width', width)
            video.setAttribute('height', height)
            canvas.setAttribute('width', width)
            canvas.setAttribute('height', height)

            streaming = true
        }
    },
    false
)

// PhotoButton Event
photoButton.addEventListener(
    'click',
    (e) => {
        takePicture()
        e.preventDefault()
    },
    false
)

// Photo Filter
photoFilter.addEventListener('change', (e) => {
    // Set filter to choosen option
    filter = e.target.value

    // set filter to video
    video.style.filter = filter

    e.preventDefault()
})

// Clear event
clearButton.addEventListener('click', (e) => {
    // Clear all photos
    photos.innerHTML = ''

    // Change filter back normal
    filter = 'none'
    video.style.filter = filter
    photoFilter.selectedIndex = 0
})

// Take picture from canvas
function takePicture() {
    const context = canvas.getContext('2d')
    if (width && height) {
        // Set Canvas props
        canvas.width = width
        canvas.height = height

        // Draw image of the video
        context.drawImage(video, 0, 0, width, height)

        // Create Image from canvas
        const imgUrl = canvas.toDataURL('image/png')

        // Create img element
        const img = document.createElement('img')
        img.setAttribute('src', imgUrl)

        // Set image filter
        img.style.filter = filter

        // Add image to photos
        photos.appendChild(img)
    }
}
